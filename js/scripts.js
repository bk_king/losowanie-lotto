(function () {
    var btn = document.querySelector('#getNumbers'),
        output = document.querySelector('#showNumbers');

    btn.addEventListener('click', randomNumbers, false);

    function getRandom(min, max) {
        return Math.floor(Math.random() * (max - min) + min);
    }

    function randomNumbers() {
        var numbers = [], i, random;
        for (i = 0; i < 6; i++) {

            random = getRandom(1, 49);
            while (numbers.indexOf(random) !== -1) {
                console.log('powtórzyła się liczba: ' + random);
                random = getRandom(1, 49);
            }
            numbers.push(random);
        }
        output.value = numbers.join(', ');
    }


})();